package com.example.mapscompose.repository

import com.example.mapscompose.data.ParkingSpotEntity
import kotlinx.coroutines.flow.Flow

interface ParkingSpotRepository {
    suspend fun insertParkingSpotRepository(spot: ParkingSpotEntity)

    suspend fun deleteParkingSpotRepository(spot: ParkingSpotEntity)

    fun getParkingSpotsRepository(): Flow<List<ParkingSpotEntity>>
}