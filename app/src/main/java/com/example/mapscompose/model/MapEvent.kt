package com.example.mapscompose.model

import com.example.mapscompose.data.ParkingSpotEntity
import com.google.android.gms.maps.model.LatLng

sealed class MapEvent {
    data class OnMapLongLClick(val latLng: LatLng) : MapEvent()
    data class OnInfoWindowLongClick(val spot: ParkingSpotEntity) : MapEvent()
}
