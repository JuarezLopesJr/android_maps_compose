package com.example.mapscompose.model

import com.example.mapscompose.data.ParkingSpotDao
import com.example.mapscompose.data.ParkingSpotEntity
import com.example.mapscompose.repository.ParkingSpotRepository
import kotlinx.coroutines.flow.Flow

class ParkingSpotRepositoryImpl(private val dao: ParkingSpotDao) : ParkingSpotRepository {
    override suspend fun insertParkingSpotRepository(spot: ParkingSpotEntity) =
        dao.insertParkingSpotDao(spot)

    override suspend fun deleteParkingSpotRepository(spot: ParkingSpotEntity) =
        dao.deleteParkingSpotDao(spot)

    override fun getParkingSpotsRepository(): Flow<List<ParkingSpotEntity>> =
        dao.getParkingSpotsDao()
}