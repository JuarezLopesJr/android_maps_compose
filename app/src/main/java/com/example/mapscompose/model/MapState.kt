package com.example.mapscompose.model

import com.example.mapscompose.data.ParkingSpotEntity
import com.google.maps.android.compose.MapProperties

data class MapState(
    val properties: MapProperties = MapProperties(),
    val parkingSpots: List<ParkingSpotEntity> = emptyList()
)
