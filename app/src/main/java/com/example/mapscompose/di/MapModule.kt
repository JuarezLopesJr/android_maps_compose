package com.example.mapscompose.di

import android.content.Context
import androidx.room.Room
import com.example.mapscompose.data.ParkingSpotDatabase
import com.example.mapscompose.model.ParkingSpotRepositoryImpl
import com.example.mapscompose.repository.ParkingSpotRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MapModule {
    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext context: Context
    ): ParkingSpotDatabase = Room.databaseBuilder(
        context,
        ParkingSpotDatabase::class.java,
        "parking_database"
    ).build()

    @Provides
    @Singleton
    fun provideRepository(db: ParkingSpotDatabase): ParkingSpotRepository =
        ParkingSpotRepositoryImpl(db.dao)
}