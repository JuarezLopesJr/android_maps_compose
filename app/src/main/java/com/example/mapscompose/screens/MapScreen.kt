package com.example.mapscompose.screens

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.mapscompose.model.MapEvent
import com.example.mapscompose.viewmodel.MapViewModel
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapUiSettings
import com.google.maps.android.compose.Marker

@Composable
fun MapScreen(mapViewModel: MapViewModel = hiltViewModel()) {
    val scaffoldState = rememberScaffoldState()
    val uiSettings = remember { MapUiSettings(zoomControlsEnabled = false) }

    Scaffold(scaffoldState = scaffoldState) {
        GoogleMap(
            modifier = Modifier.fillMaxSize(),
            properties = mapViewModel.state.properties,
            uiSettings = uiSettings,
            onMapLongClick = {
                mapViewModel.onEvent(MapEvent.OnMapLongLClick(it))
            },
            /*googleMapOptionsFactory = {
                GoogleMapOptions()
                    .camera(CameraPosition.fromLatLngZoom())
            }*/
        ) {
            mapViewModel.state.parkingSpots.forEach { spot ->
                Marker(
                    position = LatLng(spot.lat, spot.lng),
                    title = "Parking spot: ${spot.lat}, ${spot.lng}",
                    snippet = "Long click to delete",
                    onInfoWindowLongClick = {
                        mapViewModel.onEvent(MapEvent.OnInfoWindowLongClick(spot))
                    },
                    onClick = {
                        it.showInfoWindow()
                        true
                    },
                    icon = BitmapDescriptorFactory.defaultMarker(
                        BitmapDescriptorFactory.HUE_GREEN
                    )
                )
            }
        }
    }
}