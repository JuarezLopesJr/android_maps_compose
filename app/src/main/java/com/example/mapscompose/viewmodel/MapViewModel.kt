package com.example.mapscompose.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mapscompose.data.ParkingSpotEntity
import com.example.mapscompose.model.MapEvent
import com.example.mapscompose.model.MapState
import com.example.mapscompose.repository.ParkingSpotRepository
import com.google.maps.android.compose.MapProperties
import com.google.maps.android.compose.MapType
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@HiltViewModel
class MapViewModel @Inject constructor(
    private val repo: ParkingSpotRepository
) : ViewModel() {
    var state by mutableStateOf(MapState(properties = MapProperties(
        mapType = MapType.HYBRID
    )))

    init {
        viewModelScope.launch {
            repo.getParkingSpotsRepository().collectLatest {
                state = state.copy(
                    parkingSpots = it
                )
            }
        }
    }

    fun onEvent(event: MapEvent) {
        when (event) {
            is MapEvent.OnMapLongLClick -> {
                viewModelScope.launch {
                    repo.insertParkingSpotRepository(
                        ParkingSpotEntity(
                            event.latLng.latitude,
                            event.latLng.longitude
                        )
                    )
                }
            }
            is MapEvent.OnInfoWindowLongClick -> {
                viewModelScope.launch {
                    repo.deleteParkingSpotRepository(event.spot)
                }
            }
        }
    }
}